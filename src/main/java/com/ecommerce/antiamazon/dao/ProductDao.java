package com.ecommerce.antiamazon.dao;
import com.ecommerce.antiamazon.model.Product;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface ProductDao {
    public List<Product> findAll();
    public Product findById(int id);
    public Product save(Product product);

}
