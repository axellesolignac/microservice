package com.ecommerce.antiamazon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

@SpringBootApplication

public class AntiamazonApplication {

	public static void main(String[] args) throws SQLException {
		Connection dbConnection = null;
		try {
			String url = "jdbc:mysql://localhost:3306/antiamazon?serverTimezone=UTC";
				dbConnection = DriverManager.getConnection(url, "root", "");
				if (dbConnection != null) {
					System.out.println("Successfully connected to MySQL database test");
				}
			}
			catch (SQLException ex) {
				System.out.println("An error occurred while connecting MySQL database");
				ex.printStackTrace();
			}
		SpringApplication.run(AntiamazonApplication.class, args);
	}
}
